# Experiment tracking and deployment with mlflow and bentoml

## Mlflow

Helps us in logging and track experiments.

[Automatically logs](https://www.mlflow.org/docs/latest/tracking.html#automatic-logging): `Scikit-learn`, `Keras`, `Gluon`, `XGBoost`, `LightGBM`, `Statsmodels`, `Spark`, `Fastai`, `Pytorch`


Separates the following storage types:
- backend store persists MLflow entities (runs, parameters, metrics, tags, notes, metadata, etc)
- artifact store persists artifacts (files, models, images, in-memory objects, or model summary, etc

## BentoML


[placeholder]

## Current setup

- backend tracking is done with dockerized postgres (could  be any SQLAlchemy-compatible database)
- locally storing artifacts
- dockerized mlflow server
- [bentomlplaceholder]

### Steps to run:

```sh
# Rename .env.tmlp` to .env
cp .env.tmlp .env

# Fill in .env with a postgres username and password
vi .env

 mlflow ui 
 mlflow server 

# build and start the images
docker compose up --build
```

## [v0.1] postgres docker container, local mlflow

start postgres container
```sh
docker run -e POSTGRES_PASSWORD=ml_flow -e POSTGRES_USER=ml_flow --name postgres_mlflow -p 5432:5432 postgres:13
```

install `psycopg2` to enable mlflow to talk to postgres

```sh
sudo apt install libpq-dev # needed to install psycopg2
pipenv install psycopg2 # make it stay within the virtenv
```

run mlflow server
```sh
mlflow server --backend-store-uri postgresql://ml_flow:ml_flow@127.0.0.1:5432/postgres_database
```

## Troubleshooting 

While I working with docker-compose.yml, I got an error:

```
failed to solve: rpc error: code = Unknown desc = failed to solve with frontend dockerfile.v0: failed to create LLB definition: rpc error: code = Unknown desc = error getting credentials - err: exit status 1, out: ``
```

The way it was fixed:

- [optional] changed buildkit in docker desktop to `false` (not necessary)
- changed `credsStore` field to `_credsStore` in `~/.docker/config.json` (this way the field is not recognised anymore), [link](https://github.com/docker/for-win/issues/12355#issuecomment-640301591)
