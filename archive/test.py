from dotenv import load_dotenv
from mlflow.tracking import MlflowClient
import bentoml
import mlflow



import os

load_dotenv()

mlflow.set_tracking_uri(os.getenv('MLFLOW_TRACKING_URI'))

print("accessing mlflow run\n")
# run_id = "f27e7e8877fb4eda81909ee1c72007b7"
run_id = "35504b1f5a5e497ea9b71c059f9d5953"

print(mlflow.get_run(run_id))

print("\nsaving to bentoml\n")
artifact_path = "best_estimator"
# artifact_path = "model"
model_uri = f"runs:/{run_id}/{artifact_path}"
# model_uri = f"runs:/{run_id}"
bentoml.mlflow.import_model("sklearn_house_data", model_uri)
bentoml.mlflow.get("sklearn_house_data")
# bentoml.mlflow.get_mlflow_model
# bentoml.bentos.get("sklearn_house_data")
# print("\nModel imported to BentoML: %s" % bento_model)

# Save model
import mlflow
run_id = "67128244f37d4b8dbb70393b8ee5d1a7"
model_name = "iris-predicting-model"
artifact_path = "model"
model_uri = f"runs:/{run_id}/{artifact_path}"
 
model_details = mlflow.register_model(model_uri=model_uri, name=model_name)


# Retrieve once ready. After creating a model version, it may take a short period of time to become ready.
import time
from mlflow.tracking.client import MlflowClient
from mlflow.entities.model_registry.model_version_status import ModelVersionStatus
 
client = MlflowClient()

model_version_infos = client.search_model_versions(f"name = {model_name}")
new_model_version = max([model_version_info.version for model_version_info in model_version_infos])

def wait_until_ready(model_name, model_version):
  for _ in range(10):
    model_version_details = client.get_model_version(
      name=model_name,
      version=model_version,
    )
    status = ModelVersionStatus.from_string(model_version_details.status)
    print(f"Model status: {ModelVersionStatus.to_string(status)}")
    if status == ModelVersionStatus.READY:
      break
    time.sleep(1)

wait_until_ready(model_details.name, model_details.version)


