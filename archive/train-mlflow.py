from dotenv import load_dotenv
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, log_loss
import lightgbm as lgb
import mlflow
import mlflow.lightgbm
import os
import pandas as pd

from import_mlflow_bentoml import import_last_run

# Enable auto logging
load_dotenv()
mlflow.set_tracking_uri(os.getenv("MLFLOW_TRACKING_URI"))
# mlflow.set_experiment("mlflow-experiment")
mlflow.lightgbm.autolog()


def main():
    mlflow.start_run()
    run = mlflow.active_run()
    print(f"\nActive run_id: {run.info.run_id}\n")
    # Prepare training data
    df = pd.read_csv("data/iris.csv")

    flower_names = {"Setosa": 0, "Versicolor": 1, "Virginica": 2}

    X = df[["sepal.length", "sepal.width", "petal.length", "petal.width"]]
    y = df["variety"].map(flower_names)

    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.2, random_state=42
    )

    train_data = lgb.Dataset(X_train, label=y_train)

    # Train model

    params = {
        "objective": "multiclass",
        "num_class": 3,
        "learning_rate": 0.2,
        "metric": "multi_logloss",
        "feature_fraction": 0.8,
        "bagging_fraction": 0.6,
        "seed": 42,
    }

    model = lgb.train(params, train_data, valid_sets=[train_data])

    # Evaluate model
    y_proba = model.predict(X_test)
    y_pred = y_proba.argmax(axis=1)

    loss = log_loss(y_test, y_proba)
    acc = accuracy_score(y_test, y_pred)

    # Log custom metrics if you want
    mlflow.log_metrics({"log_loss": loss, "accuracy": acc})
    mlflow.end_run()
    import_last_run("iris_lgb")


if __name__ == "__main__":
    main()
