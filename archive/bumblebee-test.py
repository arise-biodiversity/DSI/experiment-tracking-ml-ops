import torch
# import mlflow.pytorch
import io
import mlflow
import os
from dotenv import load_dotenv


load_dotenv()
mlflow.set_tracking_uri(os.getenv('MLFLOW_TRACKING_URI'))

py_model = torch.jit.load('model/model.xml', map_location=torch.device('cpu'))

mlflow.pytorch.save_model(py_model, path="model/mlflow_pythorch_model")  
# model = 'model/model.bin'

# mlflow.pytorch.load_model(model)