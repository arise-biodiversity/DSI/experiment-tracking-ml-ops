# from helper.utils import fetch_logged_data
from dotenv import load_dotenv
from mlflow.tracking import MlflowClient
import bentoml
import mlflow
import numpy as np
from pprint import pprint
import pandas as pd
import os
from typing import List
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import RepeatedStratifiedKFold
from sklearn.model_selection import train_test_split

load_dotenv()

# mlflow.tracking.set_tracking_uri()
mlflow.set_tracking_uri(os.getenv('MLFLOW_TRACKING_URI'))
mlflow.set_experiment("House Sales in King County USA")

def yield_artifacts(run_id, path=None):
    """Yield all artifacts in the specified run"""
    client = MlflowClient()
    for item in client.list_artifacts(run_id, path):
        if item.is_dir:
            yield from yield_artifacts(run_id, item.path)
        else:
            yield item.path


def fetch_logged_data(run_id):
    """Fetch params, metrics, tags, and artifacts in the specified run"""
    client = MlflowClient()
    data = client.get_run(run_id).data
    # Exclude system tags: https://www.mlflow.org/docs/latest/tracking.html#system-tags
    tags = {k: v for k, v in data.tags.items() if not k.startswith("mlflow.")}
    artifacts = list(yield_artifacts(run_id))
    return {
        "params": data.params,
        "metrics": data.metrics,
        "tags": tags,
        "artifacts": artifacts,
    }
    
def split_data(csv:str) -> None:
    '''
    Separate and write the features to csv file
    '''
    df = pd.read_csv(csv)
    df = df.drop(['id', 'date'], axis=1)
    df = df.dropna()
    # split into input and output elements
    X = df.loc[:, df.columns != 'price']
    y = df.loc[:, 'price']
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33,random_state=42)
   
    X_test.to_csv('data/test/kc_house_data-X_test.csv',index=False)
    y_test.to_csv('data/test/kc_house_data-y_test.csv',index=False)
    X_train.to_csv('data/train/kc_house_data-X_train.csv',index=False)
    y_train.to_csv('data/train/kc_house_data-y_train.csv',index=False) 


def main():
    data_path = 'data/kc_house_data.csv'
    if os.path.exists(data_path):
        split_data(data_path)
    # Step 1.
    mlflow.sklearn.autolog() # supports
    try:
        input_dtypes = {
        'bedrooms': np.dtype('float64'),
        'bathrooms': np.dtype('float64'),
        'sqft_living': np.dtype('float64'),
        'sqft_lot': np.dtype('float64'),
        'floors': np.dtype('float64'),
        'waterfront': np.dtype('float64'),
        'view': np.dtype('float64'),
        'condition': np.dtype('float64'),
        'grade': np.dtype('float64'),
        'sqft_above': np.dtype('float64'),
        'sqft_basement': np.dtype('float64'),
        'yr_built': np.dtype('float64'),
        'yr_renovated': np.dtype('float64'),
        'zipcode': np.dtype('float64'),
        'lat': np.dtype('float64'),
        'long': np.dtype('float64'),
        'sqft_living15': np.dtype('float64'),
        'sqft_lot15': np.dtype('float64')
    }
        
        X_train = pd.read_csv('data/train/kc_house_data-X_train.csv', dtype=input_dtypes)
        y_train = pd.read_csv('data/train/kc_house_data-y_train.csv').values.ravel()
    except IOError:
        print('''Make sure that you have: 
                1. Downloaded the data from Kaggle into the data folder.
                2. Ran the download_data.py file''')
        return
    model = RandomForestRegressor(random_state=42)
    # define evaluation
    cv = RepeatedStratifiedKFold(n_splits=10, n_repeats=3, random_state=1)
    # define search space
    param_grid = {
        'n_estimators': [100, 300],
        'max_features': [1.0],
        'max_depth': [4, 6, 8],
        'criterion': ['squared_error']
    }
    # define search
    search = GridSearchCV(
        estimator=model, param_grid=param_grid, n_jobs=-1, cv=cv)
    # execute search
    result = search.fit(X_train, y_train)
    # summarize result
    print('Best Score: %s' % result.best_score_)
    print('Best Hyperparameters: %s' % result.best_params_)
    run_id = mlflow.last_active_run().info.run_id

    # show data logged in the parent run
    print("========== parent run ==========")
    for key, data in fetch_logged_data(run_id).items():
        print("\n---------- logged {} ----------".format(key))
        pprint(data)

    # show data logged in the child runs
    filter_child_runs = "tags.mlflow.parentRunId = '{}'".format(run_id)
    runs = mlflow.search_runs(filter_string=filter_child_runs)
    param_cols = ["params.{}".format(p) for p in param_grid.keys()]
    metric_cols = ["metrics.mean_test_score"]

    print("\n========== child runs ==========\n")
    pd.set_option("display.max_columns", None)  # prevent truncating columns
    print(runs[["run_id", *param_cols, *metric_cols]])

    # import only the best_estimator artifact to BentoML
    artifact_path = "best_estimator"
    model_uri = f"runs:/{run_id}/{artifact_path}"
    # Step 2.
    bento_model = bentoml.mlflow.import_model("sklearn_house_data", model_uri)
    print("\nModel imported to BentoML: %s" % bento_model)


if __name__ == "__main__":
    main()
