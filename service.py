from __future__ import annotations
import json
import tempfile
import datetime
import os
from collections import namedtuple
from typing import Dict, Any, List

import numpy as np
import pandas as pd
import bentoml
from bentoml.io import JSON, PandasDataFrame, Image

runner = bentoml.mlflow.get('iris-lgb').to_runner()
svc = bentoml.Service('iris-lgb', runners=[runner])

@svc.api(
    input=PandasDataFrame(),
    output=JSON(),
    route='iris/'
)
def predict(iris_data: pd.DataFrame) -> json:
    iris_data.columns = ['sepal length (cm)', 'sepal width (cm)', 'petal length (cm)', 'petal width (cm)']
    flower = runner.run(iris_data.astype(float))
    return {"classified": flower}

@svc.api(
    input=Image(),
    output=JSON(),
    route='v1/analyse/'
)
def predictions(file_input) -> json:
    """
    Analyses an image

    Expects POST parameter 'image'
    """
        
    uploaded_files = file_input
    out_json = {
        "name": "https://schemas.arise-biodiversity.nl/dsi/multi-object-multi-image#sequence-one-prediction-per-region",
        # TODO: there is no public URL with the schema yet
        "generated_by": {
            "datetime": datetime.datetime.now().isoformat() + "Z",
            "version": os.getenv("ALGORITHM_VERSION", "algorithm version unknown"),
            "tag": os.getenv("ALGORITHM_TAG", "algorithm tag unknown"),
        },
        "media": [],
        "region_groups": [],
        "predictions": [],
    }

    # -- for uploaded image perform inference
    # uploaded_file = uploaded_files[0]  # for now assume one uploaded file
    uploaded_file = uploaded_files
    # uploaded_name = uploaded_file.path
    uploaded_name = 'something.jpg'
    print(f"uploaded name: {uploaded_name}")
    ClassifiedBox = namedtuple(
        "ClassifiedBox", ["x1", "y1", "x2", "y2", "probability", "name"]
    )

    with tempfile.TemporaryDirectory() as tmp_dir:
        # write the uploaded file to temp dir
        image_path = os.path.join(tmp_dir, uploaded_name)
        uploaded_file.save(image_path)
        
        # with open(image_path, "wb") as f:
        #     f.write(uploaded_file.read())

        # TODO: perform model inference

        # TODO: add boxes/classifications from inference here
        boxes: List[ClassifiedBox] = []

        # now a hardcoded example
        boxes.append(
            ClassifiedBox(
                x1=0.20, x2=0.66, y1=0.18, y2=0.89, probability=0.95, name="organism"
            )
        )

    # ARISE output format
    media_id = os.path.splitext(os.path.split(uploaded_name)[-1])[0]
    out_json["media"].append({"filename": uploaded_name, "id": media_id})

    per_image_region_counter = 0

    for box in boxes:
        region_id = media_id + f"?region={per_image_region_counter}"

        out_json["region_groups"].append(
            {
                "id": region_id,
                # because individuals are not tracked (yet), we re-use the region_id as individual_id
                "individual_id": region_id,
                "regions": [
                    {
                        "box": {"x1": box.x1, "y1": box.y1, "x2": box.x2, "y2": box.y2},
                        "media_id": media_id,
                    }
                ],
            }
        )

        out_json["predictions"].append(
            {
                "region_group_id": region_id,
                "taxa": {
                    "type": "multiclass",
                    "items": [
                        {
                            "scientific_name": box.name,
                            "scientific_name_id": f"LITERAL:{box.name.upper()}",
                            "probability": np.round(box.probability, 6),
                        }
                    ],
                },
            }
        )

        per_image_region_counter += 1

    
    return out_json