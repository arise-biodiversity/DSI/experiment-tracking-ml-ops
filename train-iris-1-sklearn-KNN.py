from dotenv import load_dotenv
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, log_loss
import mlflow
import mlflow.sklearn
import os
import pandas as pd

# specific framework
from sklearn import metrics
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import GridSearchCV
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split

import bentoml


# Enable auto logging
load_dotenv()
mlflow.set_tracking_uri(os.getenv("MLFLOW_TRACKING_URI"))
mlflow.set_experiment("iris")
mlflow.sklearn.autolog()


def main():

    df = pd.read_csv("data/iris.csv")

    flower_names = {"Setosa": 0, "Versicolor": 1, "Virginica": 2}
    flowers = ["Setosa", "Versicolor", "Virginica"]
    X = df[["sepal.length", "sepal.width", "petal.length", "petal.width"]]
    y = df["variety"].map(flower_names)
    
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.4, random_state=42
    )

    k_range = list(range(1, 26))
    param_grid = dict(n_neighbors=k_range)
    knn = KNeighborsClassifier()
    grid = GridSearchCV(knn, param_grid, cv=10, scoring='accuracy', return_train_score=False,verbose=1, refit=True)
    
    with mlflow.start_run() as run:
        # fitting the model for grid search
        grid_search=grid.fit(X_train, y_train)
        y_pred = grid.predict_proba(X_test)
        accuracy_gridsearch = grid_search.best_score_ *100
        mlflow.log_param("flowers", flowers)
        
    run_id = mlflow.last_active_run().info.run_id
    print(run_id)
    artifact_path = "model"
    model_uri = f"runs:/{run_id}/{artifact_path}"
    bento_model = bentoml.mlflow.import_model("logistic_regression_model", model_uri)

if __name__ == "__main__":
    main()
