import os
from dotenv import load_dotenv
import numpy as np

import mlflow
import mlflow.sklearn

import bentoml

# specific framework
from sklearn import metrics
from sklearn.model_selection import train_test_split
from sklearn.datasets import load_iris
from sklearn.tree import DecisionTreeClassifier


# Enable auto logging
load_dotenv()
mlflow.set_tracking_uri(os.getenv("MLFLOW_TRACKING_URI"))
mlflow.set_experiment("iris")
mlflow.sklearn.autolog()

# Autologging must be enabled before scikit-learn metric APIs are imported from sklearn.metrics
from sklearn.metrics import accuracy_score, log_loss

def main():
    X, y = load_iris(return_X_y=True)

    # Add noisy features
    random_state = np.random.RandomState(0)
    n_samples, n_features = X.shape

    # Limit to the two first classes, and split into training and test
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.2, random_state=random_state
    )
    decision_tree = DecisionTreeClassifier(random_state=0, max_depth=2)

    with mlflow.start_run() as run:
        model = decision_tree.fit(X_train, y_train)
        y_pred = model.predict(X_test)
        # y_pred_prob = model.predict_proba(X_test)
        # y_pred_prob = y_pred_prob.argmax(axis=1)
        
        print(f"y_test:{y_test}")
        print(f"y_pred:{y_pred}")
        # print(f"y_proba:{y_pred_prob}")
        # loss = log_loss(y_test, y_pred_prob)
        acc = accuracy_score(y_test, y_pred)
        # mlflow.log_metric("log_loss", loss)
        # mlflow.log_metric("accuracy", acc)
        # metrics.classification_report(y_test,y_pred)
    

if __name__ == "__main__":
    main()