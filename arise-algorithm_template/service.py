from __future__ import annotations

import json
import numpy as np
import bentoml
import pandas as pd
import pydantic
from bentoml.io import JSON, PandasSeries, PandasDataFrame


class PreProcessor(bentoml.Runnable):
    SUPPORTED_RESOURCES = ()
    SUPPORTS_CPU_MULTI_THREADING = True

    def __init__(self):
        pass

    @bentoml.Runnable.method(batchable=False)
    def default(self, input_text: str) -> bool:
        start = time.perf_counter()
        scores = [
            self.sia.polarity_scores(sentence)["compound"]
            for sentence in nltk.sent_tokenize(input_text)
        ]
        inference_duration.labels(
            nltk_version=nltk.__version__, sentiment_cls=self.sia.__class__.__name__
        ).observe(time.perf_counter() - start)
        return mean(scores) > 0


svc = bentoml.Service('', runners=[
                       runner])


class File(pydantic.BaseModel):
    path: str


file_input = JSON(
    pydantic_model=File)

bentoctl 
@svc.api(
    input=file_input,
    output=JSON(),
    route='/v1/analyse/'
)

def predictions(file_input: File) -> json:
    uploaded_files = [{file_input: "", "filename": "test"}]
    # logger.info("Number of uploaded files: {}".format(len(uploaded_files)))
    # if len(uploaded_files) < 1:
    #     raise ApiException("No images uploaded")
    #
    out_json = {
        "name": "https://schemas.arise-biodiversity.nl/dsi/multi-object-multi-image#sequence-one-prediction-per-region",
        # TODO: there is no public URL with the schema yet
        "generated_by": {
            "datetime": datetime.datetime.now().isoformat() + "Z",
            "version": os.getenv("ALGORITHM_VERSION", "algorithm version unknown"),
            "tag": os.getenv("ALGORITHM_TAG", "algorithm tag unknown"),
        },
        "media": [],
        "region_groups": [],
        "predictions": [],
    }

    # -- for uploaded image perform inference
    uploaded_file = uploaded_files[0]  # for now assume one uploaded file
    uploaded_name = uploaded_file.filename

    ClassifiedBox = namedtuple(
        "ClassifiedBox", ["x1", "y1", "x2", "y2", "probability", "name"]
    )

    with tempfile.TemporaryDirectory() as tmp_dir:
        # write the uploaded file to temp dir
        image_path = os.path.join(tmp_dir, uploaded_name)
        with open(image_path, "wb") as f:
            f.write(uploaded_file.read())

        # TODO: perform model inference

        # TODO: add boxes/classifications from inference here
        boxes: List[ClassifiedBox] = []

        # now a hardcoded example
        boxes.append(
            ClassifiedBox(
                x1=0.20, x2=0.66, y1=0.18, y2=0.89, probability=0.95, name="organism"
            )
        )

    # ARISE output format
    media_id = os.path.splitext(os.path.split(uploaded_name)[-1])[0]
    out_json["media"].append({"filename": uploaded_name, "id": media_id})

    per_image_region_counter = 0

    for box in boxes:
        region_id = media_id + f"?region={per_image_region_counter}"

        out_json["region_groups"].append(
            {
                "id": region_id,
                # because individuals are not tracked (yet), we re-use the region_id as individual_id
                "individual_id": region_id,
                "regions": [
                    {
                        "box": {"x1": box.x1, "y1": box.y1, "x2": box.x2, "y2": box.y2},
                        "media_id": media_id,
                    }
                ],
            }
        )

        out_json["predictions"].append(
            {
                "region_group_id": region_id,
                "taxa": {
                    "type": "multiclass",
                    "items": [
                        {
                            "scientific_name": box.name,
                            "scientific_name_id": f"LITERAL:{box.name.upper()}",
                            "probability": np.round(box.probability, 6),
                        }
                    ],
                },
            }
        )

        per_image_region_counter += 1

    return flask.jsonify(out_json)

    file_input = file_input.path
    houses = pd.read_csv(file_input)
    df = preprocessor_runner.remove_na.run(houses)
    prices = runner.run(df).flatten()
    return {'prices': prices}


@svc.api(
    input=PandasDataFrame(),
    output=JSON(),
    route='/v1/analyse/'
)
def predict(house_df: pd.DataFrame) -> json:
    house_df.columns = ['bedrooms', 'bathrooms', 'sqft_living', 'sqft_lot', 'floors', 'waterfront', 'view', 'condition', 'grade',
                        'sqft_above', 'sqft_basement', 'yr_built', 'yr_renovated', 'zipcode', 'lat', 'long', 'sqft_living15', 'sqft_lot15']
    prices = runner.run(house_df.astype(float))
    return {'price': prices}
