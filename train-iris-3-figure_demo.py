import json
from dotenv import load_dotenv
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, log_loss, classification_report
import mlflow
import mlflow.sklearn
import os
import pandas as pd
import numpy as np

# specific framework
from sklearn.datasets import load_iris
from sklearn.multiclass import OneVsRestClassifier
from sklearn.metrics import precision_recall_curve
from sklearn.metrics import average_precision_score
from sklearn.preprocessing import label_binarize
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.svm import LinearSVC
import matplotlib.pyplot as plt
from itertools import cycle
from sklearn.metrics import PrecisionRecallDisplay

# Bento
# from import_mlflow_bentoml import import_last_run

load_dotenv()
mlflow.set_tracking_uri(os.getenv("MLFLOW_TRACKING_URI"))
mlflow.set_experiment("iris")
mlflow.sklearn.autolog()

def main():
    X, y = load_iris(return_X_y=True)

    # Add noisy features
    random_state = np.random.RandomState(0)
    n_samples, n_features = X.shape
    X = np.concatenate([X, random_state.randn(n_samples, 200 * n_features)], axis=1)

    # Limit to the two first classes, and split into training and test
    X_train, X_test, y_train, y_test = train_test_split(
        X[y < 2], y[y < 2], test_size=0.5, random_state=random_state
    )


    classifier = make_pipeline(StandardScaler(), LinearSVC(random_state=random_state))
    with mlflow.start_run() as run:
        classifier.fit(X_train, y_train)

        display = PrecisionRecallDisplay.from_estimator(
            classifier, X_test, y_test, name="LinearSVC"
        )
        _ = display.ax_.set_title("2-class Precision-Recall curve")
        y_score = classifier.decision_function(X_test)

        display = PrecisionRecallDisplay.from_predictions(y_test, y_score, name="LinearSVC")
        _ = display.ax_.set_title("2-class Precision-Recall curve")
        

        # Use label_binarize to be multi-label like settings
        Y = label_binarize(y, classes=[0, 1, 2])
        n_classes = Y.shape[1]

        # Split into training and test
        X_train, X_test, Y_train, Y_test = train_test_split(
            X, Y, test_size=0.5, random_state=random_state
        )
        

        classifier = OneVsRestClassifier(
            make_pipeline(StandardScaler(), LinearSVC(random_state=random_state))
        )
        classifier.fit(X_train, Y_train)
        y_score = classifier.decision_function(X_test)


        # For each class
        precision = dict()
        recall = dict()
        average_precision = dict()
        for i in range(n_classes):
            precision[i], recall[i], _ = precision_recall_curve(Y_test[:, i], y_score[:, i])
            average_precision[i] = average_precision_score(Y_test[:, i], y_score[:, i])

        # A "micro-average": quantifying score on all classes jointly
        precision["micro"], recall["micro"], _ = precision_recall_curve(
            Y_test.ravel(), y_score.ravel()
        )
        average_precision["micro"] = average_precision_score(
            Y_test, y_score, average="micro"
        )
        display = PrecisionRecallDisplay(
            recall=recall["micro"],
            precision=precision["micro"],
            average_precision=average_precision["micro"],
        )
        display.plot()
        _ = display.ax_.set_title("Micro-averaged over all classes")

        # setup plot details
        colors = cycle(["navy", "turquoise", "darkorange", "cornflowerblue", "teal"])

        _, ax = plt.subplots(figsize=(7, 8))

        f_scores = np.linspace(0.2, 0.8, num=4)
        lines, labels = [], []
        for f_score in f_scores:
            x = np.linspace(0.01, 1)
            y = f_score * x / (2 * x - f_score)
            (l,) = plt.plot(x[y >= 0], y[y >= 0], color="gray", alpha=0.2)
            plt.annotate("f1={0:0.1f}".format(f_score), xy=(0.9, y[45] + 0.02))

        display = PrecisionRecallDisplay(
            recall=recall["micro"],
            precision=precision["micro"],
            average_precision=average_precision["micro"],
        )
        display.plot(ax=ax, name="Micro-average precision-recall", color="gold")
        # mlflow.log_figure(display.plot().figure_, "Micro-average precision-recall.png")

        for i, color in zip(range(n_classes), colors):
            display = PrecisionRecallDisplay(
                recall=recall[i],
                precision=precision[i],
                average_precision=average_precision[i],
            )
            display.plot(ax=ax, name=f"Precision-recall for class {i}", color=color)
            # mlflow.log_figure(display.plot().figure_, f"Precision-recall for class-{i}.png")
            # mlflow.log_param(f"{i}-recall", recall[i])
            # mlflow.log_param(f"{i}-precision", precision[i])
            # comment the above line out to get per class

        # add the legend for the iso-f1 curves
        handles, labels = display.ax_.get_legend_handles_labels()
        handles.extend([l])
        labels.extend(["iso-f1 curves"])
        # set the legend and the axes
        ax.set_xlim([0.0, 1.0])
        ax.set_ylim([0.0, 1.05])
        ax.legend(handles=handles, labels=labels, loc="best")
        ax.set_title("Extension of Precision-Recall curve to multi-class")
        
        filename = 'figures/Extension of Precision-Recall curve to multi-class.png'
        plt.savefig(filename)
        mlflow.log_artifact(filename)
        
        # mlflow.log_figure(display.plot().figure_, "Extension of Precision-Recall curve to multi-class.jpg")
        # plt.show()

if __name__ == "__main__":
    main()
