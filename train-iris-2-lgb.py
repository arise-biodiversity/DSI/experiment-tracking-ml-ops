import json
from dotenv import load_dotenv
from sklearn.model_selection import train_test_split
import mlflow
import mlflow.lightgbm
import os
import pandas as pd
from sklearn import datasets

# specific framework
import lightgbm as lgb

# Bento
from import_mlflow_bentoml import import_last_run

load_dotenv()
mlflow.set_tracking_uri(os.getenv("MLFLOW_TRACKING_URI"))
mlflow.set_experiment("iris")
mlflow.lightgbm.autolog()
from sklearn.metrics import accuracy_score, log_loss, classification_report


def main():
    iris = datasets.load_iris()
    flowers = ["Setosa", "Versicolor", "Virginica"]
    X = pd.DataFrame(iris.data, columns=iris.feature_names)
    y = iris.target

    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.2, random_state=42
    )
    train_set = lgb.Dataset(X_train, label=y_train)
    val_set = lgb.Dataset(X_test, label=y_test)
    
    # Train model
    params = {
        "objective": "multiclass",
        "num_class": 3,
        # "learning_rate": 0.2,
        "learning_rate": 0.7,
        # "metric": "multi_logloss",
        # "metric": "multi_error",
        "metric": "auc_mu",
        "feature_fraction": 0.8,
        "bagging_fraction": 0.6,
        "seed": 42,
    }
    train_params = {
        "num_boost_round": 30,
        "verbose_eval": 5,
        "early_stopping_rounds": 5,
    }
    
    with mlflow.start_run() as run:
        model = lgb.train(
            params,
            train_set,
            valid_sets=[train_set, val_set],
            valid_names=["train", "valid"],
            **train_params,
        )

        # Evaluate model
        y_proba = model.predict(X_test)
        y_pred = y_proba.argmax(axis=1)

        loss = log_loss(y_test, y_proba)
        acc = accuracy_score(y_test, y_pred)

        # Log custom metrics if you want
        mlflow.log_metrics({"log_loss": loss, "accuracy": acc})
        

    # mlflow.end_run()
    import_last_run("iris-lgb")


if __name__ == "__main__":
    main()
