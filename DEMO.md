# Demo

Some cli commands that I might need during a demo session.

## Start
Start containers
```sh
docker compose up
```

Setup CLI environment
```sh
# launch pipenv shell to get env vars from .env
deactivate && pipenv shell

# check if mlflow env vars loaded
env | grep mlflow -i
# or
mlflow doctor
```

## Mlflow commands
```sh
mlflow gc    # delete runs in the `deleted` lifecycle 
mlflow gc --experiment-ids 1

mlflow experiments search  # search for experiments in the configured tracking server.

```

## Bento commands
```sh
bentoml build

bentoml containerize <bento_name>

docker run -it --rm -p 3000:3000 iris-lgb:gkcjjwvtjo2gaaav serve --production

# API call w/ curl for iris dataset
curl -X 'POST' \
  'http://localhost:3000/iris/' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '[[1,1.2,2.1,1.4]]'
# returns confidence for each class

# pytest w/ image input `request` for algorithm template
pytest --capture=no
```