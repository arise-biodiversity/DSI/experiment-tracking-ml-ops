import bentoml
import mlflow

def import_last_run(name: str):
    print(f"importing to bentoml")
    run_id = mlflow.last_active_run().info.run_id
    print(run_id)
    artifact_path = "model"
    model_uri = f"runs:/{run_id}/{artifact_path}"
    bentoml.mlflow.import_model(name, model_uri)
